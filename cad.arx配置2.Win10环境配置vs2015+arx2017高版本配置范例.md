- [开始](#开始)
- [几百条错误,没有安装 sdk8.0](#几百条错误没有安装-sdk80)
- [设置 x64](#设置-x64)
- [较小类型检查](#较小类型检查)
- [命令与加载卸载](#命令与加载卸载)

查看本篇之前,请看[](https://www.cnblogs.com/JJBox/p/12813502.html),因为很多资料是重复的.(强烈建议去看)

[其他版本参考 e 大的对照表](https://www.cnblogs.com/edata/p/10802746.html),自行查阅,从低到高容易,从高到低难.....

# 开始

1.1 安装:ARX2017.

C:\Autodesk\Autodesk_ObjectARX_2017_Win_64_and_32_Bit (直接双击即可,默认路径的,其他没有测试).

1.2 安装:Wizard2017(向导)

单独下载之后,放入 C:\Autodesk\Autodesk_ObjectARX_2017_Win_64_and_32_Bit\utils\

最好调整系统 UAC 设置:win+r 输入 msconfig,在工具,更 UAC,拉到最下面.

然后新建一个文本文件,并保存为 "MSI 右键添加管理员运行.reg" 的注册表文件,输入一下内容,保存后,双击这个文件执行导入注册表.

```
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Msi.Package\shell\runas]
@="以管理员运行"

[HKEY_CLASSES_ROOT\Msi.Package\shell\runas\command]
@="msiexec /i \"%1\""
```

成功导入改注册表文件之后，再 Wizard.msi 文件上右键 以管理员运行 执行安装。

1.3 在安装界面需要设置 RSD(注册开发符号,大致为 ARX 命令标识),以及 ObjectARX SDK 和 AutoCAD 程序的路径,

一般安装界面都会找到路径的默认值.此外不需要其他设置就可以完成安装.

![img](/图片/1285775-20200501142124207-163789968.png)

第 1 个路径选 1.1 中说明的路径!这里要改哟!

第 2 个路径选 CAD 所在路径,应该默认就是,点击按钮可以会出现的!

1.4 通过向导创建 ARX 项目

安装完成后,打开 Visual Studio 就可以看到 Autodesk 项目模板,选择【OK】即可进入 ObjectARX 向导.

![img](/图片/1285775-20200501142239685-1049409530.png)

1.5 项目创建异常

正常状况下,在向导界面对 ARX 项目进行适当配置即可生成项目模板.

![img](/图片/1285775-20200501142323738-428681172.png)

1.6 问题判断及解决方案(1.3 设置的方式,这里根据保留是因为防止某些人设置错误)

如果不成功,此时点击【Finish】按钮会弹出这样的警告框：

![img](/图片/1285775-20200501142338034-80224620.png)

是 arx.props 文件不存在导致的引用出错.

![img](/图片/1285775-20200501142352922-1973942217.png)

尝试重新安装 Wizard,结果 C:\ObjectARX\inc 文件夹依然缺少文件.

多次尝试发现问题如下,所谓 C:\ObjectARX\inc 文件夹实际上是安装过程中 Wizard 默认的 ObjectARX SDK 文件夹.

但如果 ObjectARX SDK 实际所在文件夹与之不一致,则会出现引用错误,而 Wizard 会自动创建一个空的 C:\ObjectARX\inc 文件夹.

【解决方案】

重新安装 Wizard,指定正确的 ObjectARX SDK 路径,如下所示.

![img](/图片/1285775-20200501142518866-83979713.png)

# 几百条错误,没有安装 sdk8.0

错误处理:

![img](/图片/1285775-20200501143813568-1705526000.png)

得到一堆错误是怎么回事....因为 vs 没有装 8.0 的 SDK(调试 2019 的时候发现的!!去控制面板更改 vs 安装下面的东西)

![img](/图片/1285775-20200501143910176-1647103190.png)

安装完成之后,可能还有 64 位编译环境的设置问题,设置一下就好了..

# 设置 x64

![img](/图片/1285775-20200501144027626-166223418.png)

# 较小类型检查

如果你现在就用 F7 生成,那么你将会得到错误,生成失败

```
D:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\include\yvals.h(112): error C2338:
/RTCc rejects conformant code, so it isn't supported by the C++ Standard Library.
Either remove this compiler option, or define _ALLOW_RTCc_IN_STL to acknowledge that you have received this warning.
```

RTCc 拒绝一致性代码，因此它不受 C++标准库支持。要么删除此编译器选项，要么定义\_ALLOW_RTCc_IN_STL 以确认您已收到此警告。这个时候你就需要设置一下.

![img](/图片/1285775-20200501144245634-1624941610.png)

![img](/图片/1285775-20200501144325071-965684427.png)

![img](/图片/1285775-20200501144339700-1186281453.png)

# 命令与加载卸载

确定,然后 F7 编译,加载你的 arx,用命令 MyCommandLocal,就会提示 hello world.

命令的位置是代码最后:

![img](/图片/1285775-20200501144414433-276603388.png)

然后按 F5,把项目文件夹下的 X64 的 arx 通过拖拉文件加载到 cad,然后输入你的命令!!

![img](/图片/1285775-20200501144432969-2008498653.png)

无法再次编译有可能是因为你 cad 已经加载了一次 arx,arx 一旦加载是加载在整个 cad 的,

不像 lisp 只是加载单个文档上,所以你必须卸载后,再编译才能成功.

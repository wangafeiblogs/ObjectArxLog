- [说明](#说明)
- [注意](#注意)
- [文件来源](#文件来源)
- [相关博客](#相关博客)

# 说明

由于 Arx 配置会随着 vs 的升级而变得不同,为了避免各类资料的陈旧而使你举步维艰,所以阿惊才有了建设此仓库的想法,并且放弃了博客的想法...

本意就是共同写出自己在实现过程中的困难.

如果你对 Arx 日志有进行修改,麻烦提交 pr,或者我拉你进来.

本工程不一定保证资料就完全正确无误,而有误的地方欢迎指正和修改,毕竟 git 的你说了算.

使用了他人博客的文件或者资料,请明确写出来源,
即使它是错误的,可以通过文字或者截图等方式来注明错误的位置,以便更好溯源.

![强行握手](/图片/强行握手.gif)

# 注意

日志中所有图片和文章引用必须修改为相对路径,以确保图片不会缺失,例如

`/图片/xxx.jpg`

`file:/../xxx.md`

对于粘贴博客到.md 时,处理已有的图片,可以在[Typora](https://typora.io/#windows)中右键图片,使用 **复制图片到...**

选择到仓库目录中的图片文件夹,再进行 ctrl+h 替换掉绝对路径前面的东西.

例如:

`!\[xxx](D:\桌面\ARX 日志\图片\xxx.jpg)`

选中: `D:\桌面\ARX 日志\` 替换为 `/`

选中: `图片\` 替换为 `图片/`

需要将 `\` 替换为 `/` 否则依然无法在 gitee 上面预览出来.

# 文件来源

| 文件                                                         | 原自于                                        |
| ------------------------------------------------------------ | --------------------------------------------- |
| objectARX 开发版本对照表.xlsx                                | https://www.cnblogs.com/edata/p/10802746.html |
| cad.arx 配置 1.Win10 环境配置 vs05+arx08 再移植到 vs10 上面再 vs19 敲代码.md | https://www.cnblogs.com/JJBox/p/12813502.html |
| cad.arx 配置 2.Win10 环境配置 vs2015+arx2017 高版本配置范例.md | https://www.cnblogs.com/JJBox/p/12813507.html |
| cad.arx 配置 3.调试,自动加载和卸载命令,学会看宏命令.md       | https://www.cnblogs.com/JJBox/p/12814876.html |
| cad.arx 自定义图元.md                                        | https://www.cnblogs.com/JJBox/p/11001275.html |
| AutoCAD ObjectARX（VC）开发基础与实例教程.rar                | 来自于张帆的书                                |
| cad 问题小百科\_Acad.md                                      | https://www.cnblogs.com/JJBox/p/10848766.html |
| arxref2020Chs.chm                                            | e大翻译的arx函数                              |
| arxdev2020chs(完成三份之一，不想继续了)                      | gzxl翻译的ObjectARX入门教程                   |

# 相关博客

e 大博客: https://www.cnblogs.com/edata

阿惊博客: https://www.cnblogs.com/JJBox